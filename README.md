# Pasex

## Description

This is a package of the Wright-Fisher-like model written as a exact Markov chain I developed with Pr. Jean-Pierre Masson.  

---

To cite this work and use the script, please mention: 

**Stoeckel S, Masson JP (2014) The Exact Distributions of FIS under Partial Asexuality in Small Finite Populations with Mutation. PLOS ONE 9(1): e85228. https://doi.org/10.1371/journal.pone.0085228**

---

CC-BY-NC-SA license, version 4: Solenn Stoeckel, INRA, 2012.